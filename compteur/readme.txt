﻿=========================================================================================
=========================================================================================
=======================		Compteur de visite V.1.0	=========================
=========================================================================================
=========================================================================================

README rédigé par Ranavy Hong
http://www.ran-dev.com



Ce compteur basique permet de compter le nombre de visite d'une page php.
Il est écrit uniquement en php et de ce fait, ne fonctionnera pas sur les fichiers html.
Il ne nécessite pas de base de données.


CONTENU DU DOSSIER
==================
L'archive compteur.zip contient un fichier test_compteur.php un dossier /compteur qui lui-même contient:
	-	/img					=>	dossier contenant les images du compteur
	-	readme.txt				=>	ce présent document
	-	cfg_compteur_visites_1.php		=>	le fichier de configuration du 1er compteur
	-	cfg_compteur_visites_2.php		=>	le fichier de configuration du 2e compteur
	-	compteur_visites.php			=>	le compteur


INSTALLATION
============
	1°)	Extraire le contenu de l'archive compteur.zip à l'aide d'un logiciel comme 7-zip, WinRar, etc...
	2°)	Placer-les directement à la racine de votre site internet.
	3°)	Au tout début de votre fichier .php :
		-	S'il s'agit de votre 1er compteur, ajouter ceci :
			<?php
				session_start();
				include("compteur/cfg_compteur_visites_1.php");
			?>
		-	S'il s'agit de votre 2e compteur, ajouter ceci :
			<?php
				session_start();
				include("compteur/cfg_compteur_visites_2.php");
			?>
	4°)	Pour insérer le compteur placer ce code :
			<?php
				include("compteur/compteur_visites.php");
			?>
			
			
IMPORTANT !!!
=============
Ce script va créer automatiquement des fichiers compteur_visites_1.txt et compteur_visites_2.txt.
Ne les modifiez surtout pas sous peine de fausser les valeurs de votre compteur.
Je ne pourrais être tenu pour responsable des risques et conséquences liés à l'utilisation de ces fichiers.

