<?php
	// Compteur de visite
	if(file_exists('compteur/compteur_visites_1.txt'))
	{
		$compteur_f = fopen('compteur/compteur_visites_1.txt', 'r+');
		$compte = fgets($compteur_f);
	}
	else
	{
		$compteur_f = fopen('compteur/compteur_visites_1.txt', 'a+');
		$compte = 0;
	}
	if(!isset($_SESSION['compteur_de_visite']))
	{
		$_SESSION['compteur_de_visite'] = 'visite';
		$compte++;
		fseek($compteur_f, 0);
		fputs($compteur_f, $compte);
	}
	fclose($compteur_f);
	
	//Création de l'affichage graphique du compteur
	$compteur_t = str_split($compte);
	$compteur_html = '';
	
	foreach($compteur_t as $chiffre){
		switch ($chiffre){
			case '0': $compteur_html = $compteur_html.'<img src="compteur/img/default/0.gif" />'; break;
			case '1': $compteur_html = $compteur_html.'<img src="compteur/img/default/1.gif" />'; break;
			case '2': $compteur_html = $compteur_html.'<img src="compteur/img/default/2.gif" />'; break;
			case '3': $compteur_html = $compteur_html.'<img src="compteur/img/default/3.gif" />'; break;
			case '4': $compteur_html = $compteur_html.'<img src="compteur/img/default/4.gif" />'; break;
			case '5': $compteur_html = $compteur_html.'<img src="compteur/img/default/5.gif" />'; break;
			case '6': $compteur_html = $compteur_html.'<img src="compteur/img/default/6.gif" />'; break;
			case '7': $compteur_html = $compteur_html.'<img src="compteur/img/default/7.gif" />'; break;
			case '8': $compteur_html = $compteur_html.'<img src="compteur/img/default/8.gif" />'; break;
			case '9': $compteur_html = $compteur_html.'<img src="compteur/img/default/9.gif" />'; break;
		}
	}
	
	// Fin affichage graphique du compteur
	// Fin compteur de visite
	?>