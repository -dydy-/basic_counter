<?php
	session_start();
	include("compteur/cfg_compteur_visites_1.php");
?>
<!DOCTYPE HTML>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Demo du compteur</title>
		<style>
			body{
				width: 900px;
				height: 600px;
				text-align: center;
				margin: 0 auto;
			}
		</style>
	</head>
	
	<body>
		<div class="title">
			<h1>Test du compteur</h1>
		</div>
		<?php 
			include("compteur/compteur_visites.php"); 
			include("compteur/cfg_compteur_visites_2.php");
			include("compteur/compteur_visites.php");
		?>
		
	</body>
</html>